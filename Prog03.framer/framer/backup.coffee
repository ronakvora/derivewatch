screen5 = Framer.Importer.load "imported/Prog03-05"
screen4 = Framer.Importer.load "imported/Prog03-04"
screen3 = Framer.Importer.load "imported/Prog03-03"
screen2 = Framer.Importer.load "imported/Prog03-02"
screen1 = Framer.Importer.load "imported/Prog03-01"

for layerGroupName of screen1
	console.log(layerGroupName)
	window[layerGroupName] = screen1[layerGroupName]

screen1layer1 = screen1['Layer 1']
screen1layer2 = screen1['Layer 2']
screen1layer1.on Events.Click, (event, layer) ->
	screen1layer2.bringToFront()
	slideOffScreen(screen1)
	
screen2layer1 = screen2['Layer 1']
screen2layer2 = screen2['Layer 2']
screen2layer3 = screen2['Layer 3']
screen2layer4 = screen2['Layer4']
screen2layer1.on Events.Click, (event, layer) ->
	screen2layer4.bringToFront()
	slideOffScreen(screen2)

screen3layer1 = screen3['Layer 1']
screen3layer2 = screen3['Layer 2']
screen3layer3 = screen3['Layer 3']
screen3layer4 = screen3['Layer 4']
x = 0
screen3layer1.on Events.Click, (event, layer) ->
	if x == 0
		screen3layer2.bringToFront()
		x = 1
	else if x == 1 
		screen3layer3.bringToFront()
		x = 2
	else if x == 2
		screen3layer4.bringToFront()
		slideOffScreen(screen3)

screen4layer1 = screen4['Layer 1']
screen4layer2 = screen4['Layer 2']
screen4layer3 = screen4['Layer 3']
screen4layer1.on Events.Click, (event, layer) ->
	screen4layer2.bringToFront()
	slideOffScreen(screen4)

screen5layer1 = screen5['Layer 1']
screen5layer2 = screen5['Layer 2']
screen5layer3 = screen5['Layer 3']
screen5layer4 = screen5['Layer 4']
y = 0
screen5layer1.on Events.Click, (event, layer) ->
	if y == 0
		screen5layer2.bringToFront()
		y = 1
	else if y == 1
		screen5layer3.bringToFront()
		slideOffScreen(screen5)

`var slideOffScreen = function (dictionary) {
	for (var key in dictionary) {
		slideOff(dictionary[key])
	}
}`

slideOff = (layer) ->
	layer.animate
		properties: {x : -640}
		time: 0.2	