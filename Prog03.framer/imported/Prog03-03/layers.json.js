window.__imported__ = window.__imported__ || {};
window.__imported__["Prog03-03/layers.json.js"] = [
	{
		"id": 44,
		"name": "Layer 4",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 4.png",
			"frame": {
				"x": 539,
				"y": 51,
				"width": 66,
				"height": 25
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "2073420466"
	},
	{
		"id": 42,
		"name": "Layer 3",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 3.png",
			"frame": {
				"x": 589,
				"y": 480,
				"width": 35,
				"height": 36
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "148623926"
	},
	{
		"id": 6,
		"name": "Layer 2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 2.png",
			"frame": {
				"x": 589,
				"y": 417,
				"width": 35,
				"height": 35
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "149338819"
	},
	{
		"id": 40,
		"name": "Layer 1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 1.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "545796590"
	}
]