window.__imported__ = window.__imported__ || {};
window.__imported__["Prog03-02/layers.json.js"] = [
	{
		"id": 59,
		"name": "Layer4",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer4.png",
			"frame": {
				"x": 0,
				"y": 243,
				"width": 640,
				"height": 66
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "930828867"
	},
	{
		"id": 5,
		"name": "Layer 3",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 3.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "492579299"
	},
	{
		"id": 8,
		"name": "Layer 2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 2.png",
			"frame": {
				"x": 194,
				"y": 141,
				"width": 233,
				"height": 38
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "491625889"
	},
	{
		"id": 57,
		"name": "Layer 1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 1.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1123
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "354676849"
	}
]