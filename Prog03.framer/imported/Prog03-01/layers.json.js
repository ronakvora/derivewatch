window.__imported__ = window.__imported__ || {};
window.__imported__["Prog03-01/layers.json.js"] = [
	{
		"id": 4,
		"name": "Layer 2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 2.png",
			"frame": {
				"x": 0,
				"y": 141,
				"width": 640,
				"height": 62
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1417451312"
	},
	{
		"id": 22,
		"name": "Layer 1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 1.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "568154796"
	}
]