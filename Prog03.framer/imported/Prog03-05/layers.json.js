window.__imported__ = window.__imported__ || {};
window.__imported__["Prog03-05/layers.json.js"] = [
	{
		"id": 4,
		"name": "Layer 4",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 4.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1931151738"
	},
	{
		"id": 35,
		"name": "Layer 3",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 3.png",
			"frame": {
				"x": 539,
				"y": 50,
				"width": 84,
				"height": 26
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1907140316"
	},
	{
		"id": 8,
		"name": "Layer 2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 2.png",
			"frame": {
				"x": 588,
				"y": 375,
				"width": 35,
				"height": 36
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1906127450"
	},
	{
		"id": 31,
		"name": "Layer 1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 1.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "177998087"
	}
]