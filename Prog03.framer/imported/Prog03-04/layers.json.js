window.__imported__ = window.__imported__ || {};
window.__imported__["Prog03-04/layers.json.js"] = [
	{
		"id": 4,
		"name": "Layer 3",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 3.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1187689509"
	},
	{
		"id": 8,
		"name": "Layer 2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 2.png",
			"frame": {
				"x": 539,
				"y": 51,
				"width": 67,
				"height": 25
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1914427093"
	},
	{
		"id": 35,
		"name": "Layer 1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 640,
			"height": 1136
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 1.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 640,
				"height": 1136
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1967056666"
	}
]