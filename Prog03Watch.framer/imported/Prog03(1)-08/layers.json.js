window.__imported__ = window.__imported__ || {};
window.__imported__["Prog03(1)-08/layers.json.js"] = [
	{
		"id": 13,
		"name": "Layer 3",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 3.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 333,
				"height": 379
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "2009632390"
	},
	{
		"id": 5,
		"name": "Layer 2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 2.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 333,
				"height": 379
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "2009573084"
	},
	{
		"id": 11,
		"name": "Layer 1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 1.png",
			"frame": {
				"x": 65,
				"y": 25,
				"width": 204,
				"height": 230
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "961535723"
	}
]