window.__imported__ = window.__imported__ || {};
window.__imported__["Prog03(1)-09/layers.json.js"] = [
	{
		"id": 5,
		"name": "Layer 2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 2.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 333,
				"height": 379
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "346143784"
	},
	{
		"id": 9,
		"name": "Layer 1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 1.png",
			"frame": {
				"x": 29,
				"y": 141,
				"width": 274,
				"height": 75
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "855993008"
	}
]