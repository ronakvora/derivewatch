window.__imported__ = window.__imported__ || {};
window.__imported__["Prog03(1)-07/layers.json.js"] = [
	{
		"id": 5,
		"name": "Layer 2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 2.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 333,
				"height": 379
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1200013446"
	},
	{
		"id": 11,
		"name": "Layer 1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 1.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 333,
				"height": 379
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "2080344533"
	}
]